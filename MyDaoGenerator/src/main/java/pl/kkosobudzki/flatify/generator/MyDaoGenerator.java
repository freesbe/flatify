package pl.kkosobudzki.flatify.generator;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;

public class MyDaoGenerator {

    public static void main(String args[]) throws Exception {
        Schema schema = new Schema(1, "pl.kkosobudzki.flatify.app");

        Entity counter = schema.addEntity("Counter");
        counter.addIdProperty();
        counter.addStringProperty("name");
        counter.addStringProperty("period");
        counter.addBooleanProperty("isConstant");
        counter.addStringProperty("unit");
        counter.addDoubleProperty("value");
        counter.addDoubleProperty("pricePerUnit");

        Entity measure = schema.addEntity("Measure");
        measure.addIdProperty();
        measure.addDoubleProperty("value");

        Property counterIdProperty = measure.addLongProperty("counterId").getProperty();
        measure.addToOne(counter, counterIdProperty);

        new DaoGenerator().generateAll(schema, args[0]);
    }
}
