package pl.kkosobudzki.flatify.app;

import pl.kkosobudzki.flatify.app.DaoSession;
import de.greenrobot.dao.DaoException;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit. 
/**
 * Entity mapped to table MEASURE.
 */
public class Measure {

    private Long id;
    private Double value;
    private Long counterId;

    /** Used to resolve relations */
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    private transient MeasureDao myDao;

    private Counter counter;
    private Long counter__resolvedKey;


    public Measure() {
    }

    public Measure(Long id) {
        this.id = id;
    }

    public Measure(Long id, Double value, Long counterId) {
        this.id = id;
        this.value = value;
        this.counterId = counterId;
    }

    /** called by internal mechanisms, do not call yourself. */
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getMeasureDao() : null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Long getCounterId() {
        return counterId;
    }

    public void setCounterId(Long counterId) {
        this.counterId = counterId;
    }

    /** To-one relationship, resolved on first access. */
    public Counter getCounter() {
        Long __key = this.counterId;
        if (counter__resolvedKey == null || !counter__resolvedKey.equals(__key)) {
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            CounterDao targetDao = daoSession.getCounterDao();
            Counter counterNew = targetDao.load(__key);
            synchronized (this) {
                counter = counterNew;
            	counter__resolvedKey = __key;
            }
        }
        return counter;
    }

    public void setCounter(Counter counter) {
        synchronized (this) {
            this.counter = counter;
            counterId = counter == null ? null : counter.getId();
            counter__resolvedKey = counterId;
        }
    }

    /** Convenient call for {@link AbstractDao#delete(Object)}. Entity must attached to an entity context. */
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }    
        myDao.delete(this);
    }

    /** Convenient call for {@link AbstractDao#update(Object)}. Entity must attached to an entity context. */
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }    
        myDao.update(this);
    }

    /** Convenient call for {@link AbstractDao#refresh(Object)}. Entity must attached to an entity context. */
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }    
        myDao.refresh(this);
    }

}
