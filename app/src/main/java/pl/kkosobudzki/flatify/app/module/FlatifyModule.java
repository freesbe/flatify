package pl.kkosobudzki.flatify.app.module;

import dagger.Module;
import dagger.Provides;
import pl.kkosobudzki.flatify.app.App;

import javax.inject.Singleton;

/**
 * @author Krzysztof Kosobudzki
 */
@Module(
        injects = {
                App.class
        },
        includes = {
                UiMortarModule.class
        },
        library = true
)
public final class FlatifyModule {
    private final App mApp;

    public FlatifyModule(App app) {
        mApp = app;
    }

    @Provides
    @Singleton
    public App provideApp() {
        return mApp;
    }
}
