package pl.kkosobudzki.flatify.app.utils;

import android.util.Log;
import pl.kkosobudzki.flatify.app.AppConfig;

/**
 * @author Krzysztof Kosobudzki
 */
public class SimpleLog {
    public static final void d(final String text, final Object... objects) {
        Log.d(AppConfig.TAG, String.format(text, objects));
    }
}
