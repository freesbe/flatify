package pl.kkosobudzki.flatify.app.model.ui;

import pl.kkosobudzki.flatify.app.Counter;

/**
 * Model for representing {@link Counter} in Ui.
 * 
 * @author Krzysztof Kosobudzki
 */
public class CounterUi {
    private final Counter mCounter;
    
    public CounterUi(final Counter counter) {
        mCounter = counter;
    }
    
    public String toString() {
        return mCounter.getName();
    }
}
