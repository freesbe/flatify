package pl.kkosobudzki.flatify.app.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import mortar.Mortar;
import mortar.MortarScope;
import pl.kkosobudzki.flatify.app.ui.blueprint.SettingsBlueprint;

import javax.inject.Inject;

/**
 * @author Krzysztof Kosobudzki
 */
public class SettingsView extends LinearLayout {
    @Inject
    SettingsBlueprint.Presenter mPresenter;

    public SettingsView(Context context, AttributeSet attrs) {
        super(context, attrs);

        // super important to inject to proper context
        MortarScope scope = Mortar.getScope(context).requireChild(new SettingsBlueprint());
        Context viewContext = scope.createContext(context);

        Mortar.inject(viewContext, this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        mPresenter.takeView(this);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        mPresenter.dropView(this);
    }
}
