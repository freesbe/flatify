package pl.kkosobudzki.flatify.app.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import pl.kkosobudzki.flatify.app.DaoMaster;
import pl.kkosobudzki.flatify.app.DaoSession;
import pl.kkosobudzki.flatify.app.utils.SimpleLog;

/**
 * @author Krzysztof Kosobudzki
 */
public class DbOpenHelper extends DaoMaster.OpenHelper {
    
    public DbOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory) {
        super(context, name, factory);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        super.onCreate(db);
        
        SimpleLog.d("DbOpenHelper onCreate db: %s", db);
        final DaoMaster daoMaster = new DaoMaster(db);
        final DaoSession daoSession = daoMaster.newSession();
        
        CounterInit.init(daoSession.getCounterDao());
    }
    
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO
        SimpleLog.d("DbOpenHelper onCreate db: %s", db);
    }
}
