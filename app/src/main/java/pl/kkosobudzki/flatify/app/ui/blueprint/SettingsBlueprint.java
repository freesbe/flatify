package pl.kkosobudzki.flatify.app.ui.blueprint;

import android.os.Bundle;
import mortar.Blueprint;
import mortar.ViewPresenter;
import pl.kkosobudzki.flatify.app.module.FlatifyModule;
import pl.kkosobudzki.flatify.app.ui.view.SettingsView;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * @author Krzysztof Kosobudzki
 */
public class SettingsBlueprint implements Blueprint {

    public SettingsBlueprint() {

    }

    @Override
    public String getMortarScopeName() {
        return getClass().getName();
    }

    @Override
    public Object getDaggerModule() {
        return new SettingsModule();
    }

    @dagger.Module(
            injects = SettingsView.class,
            addsTo = FlatifyModule.class
    )
    public class SettingsModule {

    }

    @Singleton
    public static class Presenter extends ViewPresenter<SettingsView> {

        @Inject
        public Presenter() {

        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);

            SettingsView view = getView();

            if (view != null) {
                // TODO
            }
        }

        @Override
        protected void onSave(Bundle outState) {
            super.onSave(outState);

            // TODO
        }
    }
}
