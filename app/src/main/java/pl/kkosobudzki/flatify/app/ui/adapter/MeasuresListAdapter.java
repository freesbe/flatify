package pl.kkosobudzki.flatify.app.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import pl.kkosobudzki.flatify.app.Measure;
import pl.kkosobudzki.flatify.app.R;

import java.text.DecimalFormat;

/**
 * @author Krzysztof Kosobudzki
 */
public class MeasuresListAdapter extends BaseNewBindAdapter<Measure, MeasuresListAdapter.ViewHolder> {

    private final DecimalFormat mPriceDecimalFormat = new DecimalFormat("#.##");
    private final DecimalFormat mValueDecimalFormat = new DecimalFormat("#.#");

    public MeasuresListAdapter(Context context) {
        super(context);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId();
    }

    @Override
    protected View newView(LayoutInflater inflater, ViewGroup parent) {
        View v = inflater.inflate(R.layout.view_item_measures, parent, true);

        ViewHolder viewHolder = new ViewHolder(v);
        v.setTag(viewHolder);

        return v;
    }

    @Override
    protected void bindView(Measure item, ViewHolder viewHolder) {
        viewHolder.nameTextView.setText(item.getCounter().getName());
        viewHolder.priceTextView.setText(mPriceDecimalFormat.format(item.getValue() * item.getCounter().getPricePerUnit()));
        viewHolder.quantityTextView.setText(mValueDecimalFormat.format(item.getValue()));
    }

    public static class ViewHolder {
        @InjectView(R.id.name_text_view) TextView nameTextView;
        @InjectView(R.id.price_text_view) TextView priceTextView;
        @InjectView(R.id.quantity_text_view) TextView quantityTextView;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}
