package pl.kkosobudzki.flatify.app.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Krzysztof Kosobudzki
 */
public abstract class BaseNewBindAdapter<T, S> extends BaseAdapter {
    private final List<T> mItems = new ArrayList<T>();
    private final LayoutInflater mLayoutInflater;

    protected BaseNewBindAdapter(final Context context) {
        mLayoutInflater = LayoutInflater.from(context);
    }

    public final List<T> getItems() {
        return mItems;
    }

    @Override
    public final int getCount() {
        return mItems.size();
    }

    @Override
    public final T getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public final View getView(int position, View convertView, ViewGroup parent) {
        View v;

        if (convertView == null) {
            v = newView(mLayoutInflater, parent);
        } else {
            v = convertView;
        }

        bindView(getItem(position), (S) v.getTag());

        return v;
    }

    protected abstract View newView(LayoutInflater inflater, ViewGroup parent);
    protected abstract void bindView(T item, S viewHolder);
}
