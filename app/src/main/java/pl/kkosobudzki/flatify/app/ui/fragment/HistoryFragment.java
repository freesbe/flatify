package pl.kkosobudzki.flatify.app.ui.fragment;

import pl.kkosobudzki.flatify.app.R;

/**
 * @author Krzysztof Kosobudzki
 */
public class HistoryFragment extends BaseFragment {

    public HistoryFragment() {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_month;
    }
}
