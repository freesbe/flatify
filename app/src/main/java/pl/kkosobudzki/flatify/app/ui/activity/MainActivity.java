package pl.kkosobudzki.flatify.app.ui.activity;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.*;
import android.widget.AdapterView;
import butterknife.InjectView;
import mortar.Blueprint;
import pl.kkosobudzki.common.android.fragment.DefaultFragmentSwapper;
import pl.kkosobudzki.common.android.fragment.FragmentSwapper;
import pl.kkosobudzki.flatify.app.App;
import pl.kkosobudzki.flatify.app.R;
import pl.kkosobudzki.flatify.app.ui.blueprint.MainBlueprint;
import pl.kkosobudzki.flatify.app.ui.fragment.HistoryFragment;
import pl.kkosobudzki.flatify.app.ui.fragment.SettingsFragment;
import pl.kkosobudzki.flatify.app.ui.view.DrawerView;
import pl.kkosobudzki.flatify.app.ui.view.MainView;

/**
 * @author Krzysztof Kosobudzki
 */
public class MainActivity extends BaseMortarActivity<MainActivity> {
    @InjectView(R.id.drawer_layout) MainView mMainView;
    @InjectView(R.id.drawer) DrawerView mDrawerView;
    @InjectView(R.id.app_toolbar) Toolbar mToolbar;

    private ActionBarDrawerToggle mDrawerToggle;
    private FragmentSwapper mFragmentSwapper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mFragmentSwapper = new DefaultFragmentSwapper(getFragmentManager(), R.id.content_frame);
        mFragmentSwapper.initOrRestore(savedInstanceState, HistoryFragment.class);

        mDrawerToggle = mMainView.getDrawerToggle();

        mDrawerView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                goToFragment(position);

                mMainView.closeDrawer(mDrawerView);
            }
        });

        setUpActionBar();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            // TODO

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected Blueprint getBlueprint() {
        return new MainBlueprint();
    }

    private void goToFragment(int position) {
        switch (position) {
            case 0:
                mFragmentSwapper.swapTo(HistoryFragment.class);
                break;

            case 1:
                // TODO
                break;

            case 2:
                mFragmentSwapper.swapTo(SettingsFragment.class);
                break;
        }
    }

    private void setUpActionBar() {
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }
}
