package pl.kkosobudzki.flatify.app;

import android.app.Application;
import dagger.ObjectGraph;
import mortar.Mortar;
import mortar.MortarScope;
import pl.kkosobudzki.flatify.app.module.FlatifyModule;

/**
 * @author Krzysztof Kosobudzki
 */
public class App extends Application {
    private ObjectGraph mObjectGraph;
    private MortarScope mMortarScope;

    @Override
    public void onCreate() {
        super.onCreate();

        mObjectGraph = ObjectGraph.create(new FlatifyModule(this));
        mObjectGraph.inject(this);

        mMortarScope = Mortar.createRootScope(BuildConfig.DEBUG, mObjectGraph);
    }

    public ObjectGraph getObjectGraph() {
        return mObjectGraph;
    }

    public MortarScope getRootScope() {
        return mMortarScope;
    }

    @Override
    public Object getSystemService(String name) {
        if (Mortar.isScopeSystemService(name)) {
            return mMortarScope;
        }

        return super.getSystemService(name);
    }
}
