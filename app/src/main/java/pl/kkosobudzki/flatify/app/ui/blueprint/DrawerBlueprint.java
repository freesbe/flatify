package pl.kkosobudzki.flatify.app.ui.blueprint;

import android.os.Bundle;
import mortar.Blueprint;
import mortar.ViewPresenter;
import pl.kkosobudzki.flatify.app.ui.view.DrawerView;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * @author Krzysztof Kosobudzki
 */
public class DrawerBlueprint implements Blueprint {

    @Override
    public String getMortarScopeName() {
        return getClass().getName();
    }

    @Override
    public Object getDaggerModule() {
        return new Module();
    }

    @dagger.Module(
            addsTo = MainBlueprint.Module.class,
            library = true
    )
    public final class Module {

    }

    @Singleton
    public static class Presenter extends ViewPresenter<DrawerView> {

        @Inject public Presenter() {

        }

        @Override
        public void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);

            if (getView() == null) {
                return;
            }

            getView().setListAdapter();
        }
    }
}
