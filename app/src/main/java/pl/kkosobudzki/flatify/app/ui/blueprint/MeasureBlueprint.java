package pl.kkosobudzki.flatify.app.ui.blueprint;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import mortar.Blueprint;
import mortar.ViewPresenter;
import pl.kkosobudzki.flatify.app.CounterDao;
import pl.kkosobudzki.flatify.app.model.ui.CounterUi;
import pl.kkosobudzki.flatify.app.module.DaoModule;
import pl.kkosobudzki.flatify.app.ui.activity.NewMeasureActivity;
import pl.kkosobudzki.flatify.app.ui.view.MeasureView;
import pl.kkosobudzki.flatify.app.utils.SimpleLog;
import rx.Observable;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Measure screen.
 *
 * @author Krzysztof Kosobudzki
 */
public class MeasureBlueprint implements Blueprint {

    @Override
    public String getMortarScopeName() {
        return getClass().getName();
    }

    @Override
    public Object getDaggerModule() {
        return new Module();
    }

    @dagger.Module(
            injects = {
                    MeasureView.class,
                    NewMeasureActivity.class
            },
            includes = {
                    DaoModule.class
            },
            library = true
    )
    public class Module {

    }

    @Singleton
    public static class Presenter extends ViewPresenter<MeasureView> {
        private CounterDao mCounterDao;
        
        @Inject
        public Presenter(CounterDao counterDao) {
            SimpleLog.d("MeasurePresenter counterDao: %s", counterDao);
            mCounterDao = counterDao;
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);

            if (getView() != null) {
                ArrayAdapter<CounterUi> adapter = new ArrayAdapter<CounterUi>(getView().getContext(), android.R.layout.simple_spinner_item);

                Observable.from(mCounterDao.loadAll())
                        .map(item -> new CounterUi(item))
                        .toList()
                        .subscribe(item -> {
                            adapter.addAll(item);

                            getView().setCounterAdapter(adapter);
                        });
            }
        }

        @Override
        protected void onSave(Bundle outState) {
            super.onSave(outState);

            // TODO
        }
    }
}
