package pl.kkosobudzki.flatify.app.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnItemClick;
import com.melnykov.fab.FloatingActionButton;
import mortar.Mortar;
import mortar.MortarScope;
import pl.kkosobudzki.flatify.app.R;
import pl.kkosobudzki.flatify.app.model.Types;
import pl.kkosobudzki.flatify.app.ui.blueprint.MonthBlueprint;
import pl.kkosobudzki.flatify.app.utils.SimpleLog;

import javax.inject.Inject;
import java.text.DecimalFormat;

/**
 * @author Krzysztof Kosobudzki
 */
public class MonthView extends FrameLayout {
    @Inject MonthBlueprint.Presenter mPresenter;

    @InjectView(R.id.measures_list_view) ListView mListView;
    @InjectView(R.id.add_measure_button) FloatingActionButton mAddMeasureButton;

    private Context mContext;

    private TextView mTotalTextView;
    private TextView mStatusTextView;

    private final DecimalFormat mDecimalFormat = new DecimalFormat("#");

    public MonthView(Context context, AttributeSet attrs) {
        super(context, attrs);

        // super important to inject to proper context
        MortarScope scope = Mortar.getScope(context).requireChild(new MonthBlueprint());
        mContext = scope.createContext(context);

        Mortar.inject(mContext, this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        ButterKnife.inject(this);

        mPresenter.takeView(this);

        setUpView();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        mPresenter.dropView(this);
    }

    @OnClick(R.id.add_measure_button)
    public void onClickAddMeasure() {
        mPresenter.addMeasure();
    }

    @OnItemClick(R.id.measures_list_view)
    public void onItemClickMeasures() {
        // TODO
//        mPresenter.onMeasureSelected();
    }


    private void setUpView() {
        mAddMeasureButton.attachToListView(mListView);
    }

    private View setUpHeader() {
        final View headerView = LayoutInflater.from(mContext).inflate(R.layout.view_header_measures, null, false);

        mTotalTextView = (TextView) headerView.findViewById(R.id.total_text_view);
        mStatusTextView = (TextView) headerView.findViewById(R.id.status_text_view);

        return headerView;
    }

    public void clean() {
        mTotalTextView = null;
        mStatusTextView = null;

        mContext = null;
    }

    public void setListAdapter(ListAdapter listAdapter) {
        mListView.addHeaderView(setUpHeader());
        mListView.setAdapter(listAdapter);

        SimpleLog.d("MonthView setListAdapter");
    }

    public void setTotal(double total) {
        if (mTotalTextView != null) {
            mTotalTextView.setText(mDecimalFormat.format(total));
        }
    }

    public void setStatus(Types.Status status) {
        if (mStatusTextView != null) {
            mStatusTextView.setText(status.toString());

            // TODO
        }
    }
}
