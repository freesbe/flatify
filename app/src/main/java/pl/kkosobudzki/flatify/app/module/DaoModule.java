package pl.kkosobudzki.flatify.app.module;

import android.database.sqlite.SQLiteDatabase;
import dagger.Module;
import dagger.Provides;
import pl.kkosobudzki.flatify.app.*;
import pl.kkosobudzki.flatify.app.db.DbOpenHelper;

import javax.inject.Singleton;

/**
 * @author Krzysztof Kosobudzki
 */
@Module(
        addsTo = FlatifyModule.class,
        library = true
)
public class DaoModule {
    private static final String DB_NAME = "flatify-db";

    @Provides
    @Singleton
    DaoSession provideDaoSession(App app) {
        final DaoMaster.OpenHelper helper = new DbOpenHelper(app, DB_NAME, null);
        final SQLiteDatabase db = helper.getWritableDatabase();
        final DaoMaster daoMaster = new DaoMaster(db);

        return daoMaster.newSession();
    }

    @Provides
    @Singleton
    MeasureDao provideMeasureDao(DaoSession daoSession) {
        return daoSession.getMeasureDao();
    }
    
    @Provides
    @Singleton
    CounterDao provideCounterDao(DaoSession daoSession) {
        return daoSession.getCounterDao();
    }
}
