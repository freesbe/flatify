package pl.kkosobudzki.flatify.app.ui.blueprint;

import android.os.Bundle;
import android.widget.ListAdapter;
import mortar.Blueprint;
import mortar.ViewPresenter;
import pl.kkosobudzki.flatify.app.Measure;
import pl.kkosobudzki.flatify.app.MeasureDao;
import pl.kkosobudzki.flatify.app.model.Types;
import pl.kkosobudzki.flatify.app.module.DaoModule;
import pl.kkosobudzki.flatify.app.ui.Navigator;
import pl.kkosobudzki.flatify.app.ui.adapter.MeasuresListAdapter;
import pl.kkosobudzki.flatify.app.ui.view.MonthView;
import pl.kkosobudzki.flatify.app.utils.SimpleLog;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Month screen. Contains all measurements for month.
 *
 * @author Krzysztof Kosobudzki
 */
public class MonthBlueprint implements Blueprint {

    @Override
    public String getMortarScopeName() {
        return getClass().getName();
    }

    @Override
    public Object getDaggerModule() {
        return new Module();
    }

    @dagger.Module(
            injects = {
                    MonthView.class
            },
            includes = {
                    DaoModule.class
            },
            library = true
    )
    public class Module {

    }

    @Singleton
    public static class Presenter extends ViewPresenter<MonthView> {

        @Inject
        public Presenter(MeasureDao measureDao) {
            SimpleLog.d("MonthPresenter measureDao: %s", measureDao);
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);

            if (getView() != null) {
                fetchAndSetData();
            }
        }

        @Override
        protected void onSave(Bundle outState) {
            super.onSave(outState);

            getView().clean();
        }

        public void addMeasure() {
            // TODO go to fragment
            SimpleLog.d("MonthBlueprint add measure");

            Navigator.newMeasure(getView().getContext());
        }

        public void onMeasureSelected(Measure measure) {
            // TODO go to fragment
            SimpleLog.d("MonthBlueprint on measure selected, measure: %d", measure);
        }

        private void fetchAndSetData() {
            // TODO rxjava fetchdata and subsribe

            ListAdapter adapter = new MeasuresListAdapter(getView().getContext());

            // TODO tmp
            getView().setStatus(Types.Status.PAID);
            getView().setTotal(1432.2345d);
            getView().setListAdapter(adapter);
        }
    }
}
