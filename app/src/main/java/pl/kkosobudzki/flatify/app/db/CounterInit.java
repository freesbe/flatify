package pl.kkosobudzki.flatify.app.db;

import pl.kkosobudzki.flatify.app.Counter;
import pl.kkosobudzki.flatify.app.CounterDao;
import pl.kkosobudzki.flatify.app.utils.SimpleLog;

/**
 * Helper class to insert initial data.
 * 
 * * * @author Krzysztof Kosobudzki
 */
public class CounterInit {
    public static final void init(final CounterDao dao) {
        long eneryId = dao.insert(getEnergyCounter());
        long coldWaterId = dao.insert(getColdWaterCounter());
        long hotWaterId = dao.insert(getHotWaterCounter());

        SimpleLog.d("Energy id: %d, cold water id: %d, hot water id: %d", eneryId, coldWaterId, hotWaterId);
    }
    
    private static final Counter getEnergyCounter() {
        // TODO strings from resources
        // TODO initial values from "tutorial"
        final Counter energy = new Counter();
        energy.setName("Energy");
        energy.setPeriod("month");
        energy.setIsConstant(false);
        energy.setUnit("kW");
        energy.setValue(0.0d);
        energy.setPricePerUnit(123.45d);
        
        return energy;
    }

    private static final Counter getColdWaterCounter() {
        // TODO strings from resources
        // TODO initial values from "tutorial"
        final Counter coldWater = new Counter();
        coldWater.setName("Cold water");
        coldWater.setPeriod("month");
        coldWater.setIsConstant(false);
        coldWater.setUnit("m3");
        coldWater.setValue(0.0d);
        coldWater.setPricePerUnit(14.87d);

        return coldWater;
    }

    private static final Counter getHotWaterCounter() {
        // TODO strings from resources
        // TODO initial values from "tutorial"
        final Counter hotWater = new Counter();
        hotWater.setName("Hot water");
        hotWater.setPeriod("month");
        hotWater.setIsConstant(false);
        hotWater.setUnit("m3");
        hotWater.setValue(0.0d);
        hotWater.setPricePerUnit(42.55d);

        return hotWater;
    }
}
