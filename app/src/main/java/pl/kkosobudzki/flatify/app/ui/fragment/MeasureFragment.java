package pl.kkosobudzki.flatify.app.ui.fragment;

import pl.kkosobudzki.flatify.app.R;

/**
 * @author Krzysztof Kosobudzki
 */
public class MeasureFragment extends BaseFragment {

    public MeasureFragment() {
        // TODO action bar
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_measure;
    }
}
