package pl.kkosobudzki.flatify.app.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import butterknife.ButterKnife;
import mortar.Mortar;
import pl.kkosobudzki.flatify.app.R;
import pl.kkosobudzki.flatify.app.ui.blueprint.DrawerBlueprint;

import javax.inject.Inject;

/**
 * @author Krzysztof Kosobudzki
 */
public class DrawerView extends ListView {

    @Inject DrawerBlueprint.Presenter mPresenter;

    public DrawerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);

        Mortar.inject(context, this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        ButterKnife.inject(this);

        mPresenter.takeView(this);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        mPresenter.dropView(this);
    }

    public void setListAdapter() {
        setAdapter(new ArrayAdapter<>(
                getContext(),
                android.R.layout.simple_list_item_activated_1,
                android.R.id.text1,
                getContext().getResources().getStringArray(R.array.drawer_items)
        ));
    }
}
