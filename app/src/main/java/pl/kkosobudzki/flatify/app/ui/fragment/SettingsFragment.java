package pl.kkosobudzki.flatify.app.ui.fragment;

import pl.kkosobudzki.flatify.app.R;

/**
 * @author Krzysztof Kosobudzki
 */
public class SettingsFragment extends BaseFragment {

    public SettingsFragment() {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_settings;
    }
}
