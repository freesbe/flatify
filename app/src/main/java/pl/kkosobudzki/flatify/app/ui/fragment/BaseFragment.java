package pl.kkosobudzki.flatify.app.ui.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * @author Krzysztof Kosobudzki
 */
public abstract  class BaseFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO use savedInstanceState
        return inflater.inflate(getLayoutId(), container, false);
    }

    protected abstract int getLayoutId();
}
