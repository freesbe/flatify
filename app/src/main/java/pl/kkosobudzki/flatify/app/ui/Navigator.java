package pl.kkosobudzki.flatify.app.ui;

import android.content.Context;
import android.content.Intent;
import pl.kkosobudzki.flatify.app.ui.activity.NewMeasureActivity;

/**
 * @author Krzysztof Kosobudzki
 */
public class Navigator {
    public static final void newMeasure(final Context context) {
        Intent intent = new Intent(context, NewMeasureActivity.class);

        context.startActivity(intent);
    }
}
