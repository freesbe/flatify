package pl.kkosobudzki.flatify.app.ui.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import butterknife.ButterKnife;
import dagger.ObjectGraph;
import mortar.Blueprint;
import mortar.Mortar;
import mortar.MortarActivityScope;
import mortar.MortarScope;
import pl.kkosobudzki.flatify.app.App;
import pl.kkosobudzki.flatify.app.BuildConfig;
import pl.kkosobudzki.flatify.app.module.UiMortarModule;

/**
 * Base activity to avoid all Mortar boilerplate code.
 *
 * @author Krzysztof Kosobudzki
 */
public abstract class BaseMortarActivity<T> extends ActionBarActivity {
    private MortarActivityScope mActivityScope;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MortarScope parentScope = ((App) getApplication()).getRootScope();
        
        mActivityScope = Mortar.requireActivityScope(parentScope, getBlueprint());
        mActivityScope.onCreate(savedInstanceState);

        Mortar.inject(this, (T) this);

        setContentView(getLayoutId());

        ButterKnife.inject(this);
    }

    @Override
    public Object getSystemService(String name) {
        if (Mortar.isScopeSystemService(name)) {
            return mActivityScope;
        }

        return super.getSystemService(name);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        mActivityScope.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (isFinishing() && mActivityScope != null) {
            MortarScope parentScope = ((App) getApplication()).getRootScope();
            parentScope.destroyChild(mActivityScope);

            mActivityScope = null;
        }
    }

    protected abstract int getLayoutId();
    protected abstract Blueprint getBlueprint();
}
