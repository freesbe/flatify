package pl.kkosobudzki.flatify.app.ui.view;

import android.app.Activity;
import android.content.Context;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ListView;
import butterknife.InjectView;
import mortar.Mortar;
import pl.kkosobudzki.flatify.app.R;
import pl.kkosobudzki.flatify.app.ui.blueprint.MainBlueprint;

import javax.inject.Inject;

/**
 * @author Krzysztof Kosobudzki
 */
public class MainView extends DrawerLayout {
    @InjectView(R.id.app_toolbar) Toolbar mToolbar;
    @InjectView(R.id.drawer) ListView mDrawerList;

    @Inject MainBlueprint.Presenter mPresenter;

    private ActionBarDrawerToggle mDrawerToggle;

    public MainView(Context context, AttributeSet attrs) {
        super(context, attrs);

        Mortar.inject(context, this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        mPresenter.takeView(this);

        initNavigationDrawer();
    }


    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        mPresenter.dropView(this);
    }

    public void initNavigationDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle((Activity) getContext(), this, mToolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };

        post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

        setDrawerListener(mDrawerToggle);
    }

    public ActionBarDrawerToggle getDrawerToggle() {
        return mDrawerToggle;
    }
}
