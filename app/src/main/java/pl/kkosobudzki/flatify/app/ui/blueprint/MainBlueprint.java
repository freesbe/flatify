package pl.kkosobudzki.flatify.app.ui.blueprint;

import mortar.Blueprint;
import mortar.ViewPresenter;
import pl.kkosobudzki.flatify.app.module.FlatifyModule;
import pl.kkosobudzki.flatify.app.module.UiMortarModule;
import pl.kkosobudzki.flatify.app.ui.view.DrawerView;
import pl.kkosobudzki.flatify.app.ui.view.MainView;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * @author Krzysztof Kosobudzki
 */
public class MainBlueprint implements Blueprint {

    public MainBlueprint() {

    }

    @Override
    public String getMortarScopeName() {
        return getClass().getName();
    }

    @Override
    public Object getDaggerModule() {
        return new Module();
    }

    @dagger.Module(
            injects = {
                    MainView.class,
                    DrawerView.class
            },
            addsTo = FlatifyModule.class
    )
    public final class Module {

    }

    @Singleton
    public static class Presenter extends ViewPresenter<MainView> {

        @Inject
        Presenter() {

        }

        protected Blueprint getFirstScreen() {
            return new MonthBlueprint();
        }

        protected Blueprint getDrawerScreen() {
            return new DrawerBlueprint();
        }
    }
}
