package pl.kkosobudzki.flatify.app.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.*;
import butterknife.ButterKnife;
import butterknife.InjectView;
import mortar.Mortar;
import pl.kkosobudzki.flatify.app.Counter;
import pl.kkosobudzki.flatify.app.R;
import pl.kkosobudzki.flatify.app.model.ui.CounterUi;
import pl.kkosobudzki.flatify.app.ui.blueprint.MeasureBlueprint;

import javax.inject.Inject;

/**
 * @author Krzysztof Kosobudzki
 */
public class MeasureView extends LinearLayout {
    @Inject MeasureBlueprint.Presenter mPresenter;

    @InjectView(R.id.new_measure_value) EditText mValueEditText;
    @InjectView(R.id.new_measure_type) Spinner mTypeSpinner;

    public MeasureView(Context context, AttributeSet attrs) {
        super(context, attrs);

        Mortar.inject(context, this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        ButterKnife.inject(this);

        mPresenter.takeView(this);

        setUpView();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        mPresenter.dropView(this);
    }
    
    private void setUpView() {
        // TODO
    }
    
    public void setCounterAdapter(ArrayAdapter<CounterUi> adapter) {
        mTypeSpinner.setAdapter(adapter);
    }
}
