package pl.kkosobudzki.flatify.app.ui.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import butterknife.InjectView;
import mortar.Blueprint;
import pl.kkosobudzki.flatify.app.R;
import pl.kkosobudzki.flatify.app.ui.blueprint.MeasureBlueprint;

/**
 * @author Krzysztof Kosobudzki
 */
public class NewMeasureActivity extends BaseMortarActivity {

    @InjectView(R.id.app_toolbar) Toolbar mToolbar;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setUpActionBar();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_new_measure;
    }

    @Override
    protected Blueprint getBlueprint() {
        return new MeasureBlueprint();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.new_measure, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            
            case R.id.save:
                // TODO
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setUpActionBar() {
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(android.R.drawable.ic_menu_close_clear_cancel);
    }
}
