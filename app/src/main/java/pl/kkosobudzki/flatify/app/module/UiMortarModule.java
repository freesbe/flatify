package pl.kkosobudzki.flatify.app.module;

import dagger.Module;
import pl.kkosobudzki.flatify.app.ui.activity.MainActivity;

/**
 * @author Krzysztof Kosobudzki
 */
@Module(
        injects = {
                MainActivity.class
        }
)
public class UiMortarModule {

}
